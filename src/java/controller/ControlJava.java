/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;


/**
 *
 * @author ICIT
 */
@Controller
@RequestMapping("/main")
public class ControlJava {
    @RequestMapping("/emailfinder")
    public ModelAndView emailfinder(){
        return new ModelAndView("testajax");
    }
    
    @RequestMapping(value = "/testajax", method = RequestMethod.POST)
    public @ResponseBody String ajaxCall(@RequestParam("email") String email_id, @RequestParam("mynum") String num1) {
        return "Hello I am from AJax response " + email_id + " " + num1;
    }
}
