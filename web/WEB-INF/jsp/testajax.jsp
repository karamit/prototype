<%-- 
    Document   : testajax
    Created on : Nov 24, 2016, 8:59:35 PM
    Author     : ICIT
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <div>
            <button type="submit" id="checkbutton" onclick="ajaxcall();">Click Me</button>
            <div>
                <input type="text" id="email">
                <p id="showtext"> </p>
            </div>
        </div>
        <h1>Hello World!</h1>
        
    </body>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script>
           function ajaxcall(){
               
               $.ajax({
                   url: "<c:url value="testajax.htm"/>",
                   method: "post",
                   data: {email: $('#email').val(), mynum : 12345},
                   success: function(data){
                       console.log(data)
                       console.log("success ");
                   }
                   
               });
           } 
        </script>
</html>
